package main

import (
	"fmt"
	"google.golang.org/api/iterator"
	"log"

	"cloud.google.com/go/datastore"
)

func query() {
	fmt.Println("query")

	query := datastore.NewQuery("msg")
	it := client.Run(Ctxx, query)
	for {
		var msg Msg
		_, err := it.Next(&msg)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Error fetching next task: %v", err)
		}
		fmt.Printf("Email %q, Content %q\n %q", msg.Email, msg.Content, msg.Key)
	}
}
