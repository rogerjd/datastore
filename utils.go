package main

import (
	"bufio"
	"os"
	"strings"
)

func ConsoleReadLn() string {
	rdr := bufio.NewReader(os.Stdin)
	txt, err := rdr.ReadString('\n')
	if err == nil {
		return strings.TrimRight(txt, "\n\r")
	}
	return ""
}
