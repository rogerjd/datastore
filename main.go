package main

//https://godoc.org/cloud.google.com/go/datastore

/*
	1 - Start the emulator
		gcloud beta emulators datastore start [flags]
	2 - Setting environment variables
		in a cmd window prompt that is open on the prj folder (w/ app.yaml), enter
		gcloud beta emulators datastore env-init > set_vars.cmd && set_vars.cmd
	3 - run the go pgm from the same cmd prompt/window
		go run .

*/

import (
	"cloud.google.com/go/datastore"
	"context"
	"fmt"
	"log"
	"os"
)

type Task struct {
	Key         *datastore.Key `datastore:"__key__"`
	Description string
}

type Msg struct {
	Key     *datastore.Key `datastore:"__key__"`
	Email   string
	Content string
	Errors  map[string]string `datastore:"-"`
}

var (
	Ctxx   context.Context
	client *datastore.Client

	N int = 3
)

func main() {
	fmt.Println("Hello")
	println(os.Getenv("DATASTORE_EMULATOR_HOST"))
	fmt.Println(len(os.Args), os.Args[0])

	Ctxx = context.Background()

	x1 := datastore.IDKey("Tesk", 0, nil)
	fmt.Println(x1)
	x2 := datastore.IncompleteKey("Task", nil)
	fmt.Println(x2)

	prjID := "test"
	//	client, err := storage.NewClient(Ctx, option.WithCredentials(creds))

	fmt.Print(N, &N)
	println()
	N := 5
	fmt.Println(N, &N)
	var err error
	client, err = datastore.NewClient(Ctxx, prjID)
	if err != nil {
		log.Fatalf("Faile to create client: %v", err)
	}

	fmt.Printf("enter val: ")
	txt := ConsoleReadLn()
	switch txt {
	case "gp":
		GetPut()
	case "q":
		query()
	case "key":
		key()
	case "eg":
		entityGroups()
	case "apt":
		addPetAndToy()
	case "qt":
		qt()
	case "nscc":
		nsCreateCompany()
	case "nsq":
		nsQuery()
	case "nsccko":
		nsCreateCompanyKeyOnly()
	case "nsqko":
		nsQueryKeyOnly()
	case "nsm":
		nsMetadata()
	case "nsgpk":
		nsGetPutkey()
	}

	/*
		creds, err := google.FindDefaultCredentials(Ctx, storage.ScopeReadOnly)
		if err != nil {
				log.Fatal(err)
		}
	*/
}
