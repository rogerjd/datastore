package main

//namespace
//https://stackoverflow.com/questions/47174559/how-to-query-a-entity-from-datastore-with-namespace-in-golang

/*
	12/8/19 here is i believe the right way:
		- W, Key.Namespace = "val" "" is default
		- R  "			"				"
			Query.Namespace(val)
*/

import (
	"cloud.google.com/go/datastore"
	"context"
	"fmt"
	"google.golang.org/api/iterator"
	_ "google.golang.org/appengine"
	"log"
)

type (
	Company struct {
		Name string
	}

	Emp struct {
		Name string
	}

	PayCheck struct {
		Gross float32
	}
)

func nsCreateCompany() {
	fmt.Println("ns Create Company")
	ck := datastore.NameKey("Company", "abc2", nil) //ns should agree: ctx and key. both ??? ctx & key
	ck.Namespace = "abc"
	c := Company{"Abc"}
	//Ctxx, err := appengine.Namespace(context.Background(), "abc") //ck.Namespace
	k, err := client.Put(Ctxx, ck, &c)
	log.Println(err, k, k.Incomplete(), c, Ctxx)

	ck = datastore.NameKey("Company", "xyz2", nil)
	ck.Namespace = "xyz"
	c = Company{"Xyz"}
	//Ctxx, err = appengine.Namespace(context.Background(), ck.Namespace)
	k, err = client.Put(Ctxx, ck, &c)
	log.Println(err, k, k.Incomplete(), c, Ctxx)
}

func nsQuery() {
	fmt.Println("ns Query Company")

	//default namespace, expect 0
	//abc ns expect 1
	//xyz ns expect 1

	query := datastore.NewQuery("Company")
	it := client.Run(context.Background(), query)
	for {
		var comp Company
		k, err := it.Next(&comp)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Error fetching next task: %v", err)
		}
		fmt.Printf("Name %q\n", comp.Name, k, k.Namespace)
	}

	fmt.Println("ns xyz")
	query2 := datastore.NewQuery("Company").Namespace("xyz")
	//Ctxx, err := appengine.Namespace(context.Background(), "abc") //for query R, dont use this it seems, use Query.Namespace()
	fmt.Println(Ctxx)
	/*
		if err != nil {
			log.Fatal(err)
		}
	*/
	it2 := client.Run(Ctxx, query2)
	for {
		var comp Company
		k, err := it2.Next(&comp)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Error fetching next task: %v", err)
		}
		fmt.Printf("Name %q %v, %s\n", comp.Name, k, k.Namespace)
	}
}

func nsCreateCompanyKeyOnly() {
	fmt.Println("ns Create Company ko")
	ck := datastore.NameKey("Company", "abcko", nil) //ns should agree: ctx and key. both ??? ctx & key
	ck.Namespace = "abc"
	c := Company{"Abc key only 2"}
	//	Ctxx, err := appengine.Namespace(context.Background(), "abc") // i dont think we need this, just use Key.Namespace
	k, err := client.Put(Ctxx, ck, &c)
	log.Println(err, k, k.Incomplete(), c, Ctxx)

	ck = datastore.NameKey("Company", "xyzko", nil)
	ck.Namespace = "xyz"
	c = Company{"Xyz key only 2"}
	//	Ctxx, err = appengine.Namespace(context.Background(), ck.Namespace)
	k, err = client.Put(Ctxx, ck, &c)
	log.Println(err, k, k.Incomplete(), c, Ctxx)
}

//use Query.Namespace() to specifiy ns, not appengine.Namespace(ctx
func nsQueryKeyOnly() {
	fmt.Println("ns Query Company key only")

	//default namespace, expect 0
	//abc ns expect 1
	//xyz ns expect 1

	query := datastore.NewQuery("Company")
	it := client.Run(context.Background(), query)
	for {
		var comp Company
		_, err := it.Next(&comp)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Error fetching next task: %v", err)
		}
		fmt.Printf("Name %q\n", comp.Name)
	}

	fmt.Println("ns abc ko")
	query2 := datastore.NewQuery("Company").Namespace("abc")
	//	Ctxx, err := appengine.Namespace(context.Background(), "abc") //for query R, dont use this it seems, use Query.Namespace()
	/*
		fmt.Println(Ctxx)
			if err != nil {
				log.Fatal(err)
			}
	*/
	it2 := client.Run(Ctxx, query2)
	for {
		var comp Company
		_, err := it2.Next(&comp)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Error fetching next task: %v", err)
		}
		fmt.Printf("Name %q\n", comp.Name)
	}
}

//also can query types
func nsMetadata() {
	//start := datastore.NameKey("__namespace__", "a", nil)
	q := datastore.NewQuery("__namespace__").KeysOnly()
	keys, err := client.GetAll(context.Background(), q, nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("namespaces:")
	for i, k := range keys {
		fmt.Println(i, k.Name, k.Namespace)
	}
}

//test namespace, using Key.Namespace and Query.Namespace, only (not ctx)
func nsGetPutkey() {
	fmt.Println("ns Get Put, key")

	ck := datastore.NameKey("Company", "tst1", nil) //ns should agree: ctx and key. both ??? ctx & key
	ck.Namespace = "abc"
	c := Company{"Comp1-abc"}
	//Ctxx, err := appengine.Namespace(context.Background(), "abc") //ck.Namespace
	k, err := client.Put(Ctxx, ck, &c)
	log.Println(err, k, k.Incomplete(), c, Ctxx)

	ck = datastore.NameKey("Company", "tst1", nil) //same key
	ck.Namespace = "xyz"
	c = Company{"Comp2-xyz"}
	//	Ctxx, err = appengine.Namespace(context.Background(), ck.Namespace)
	k2, err := client.Put(Ctxx, ck, &c)
	log.Println(err, k2, k2.Incomplete(), c, Ctxx)

	c1 := Company{}
	client.Get(Ctxx, k, &c1)
	fmt.Println(k, c1, k.Namespace)

	c2 := Company{}
	client.Get(Ctxx, k2, &c2)
	fmt.Println(k2, c2, k2.Namespace)
}

/* i think this is outdated, doesnt compile: undefined: "cloud.google.com/go/datastore".NewKey
type CompCtx struct{ Name string }

func nsCtxTest() {
	ctx := context.Background()
	c1 := CompCtx{"default namespace"}
	key := datastore.NewKey(ctx, "CompCtx", "", 0, nil)
	key, err := datastore.Put(CompCtx, key, &c1)	
}
*/