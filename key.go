package main

import (
	"cloud.google.com/go/datastore"
	"fmt"
	"log"
)

func key() {
	fmt.Println("key")
	//
	k := datastore.IncompleteKey("tst", nil)
	fmt.Println("Incomplete:", k.Incomplete())

	k = datastore.IDKey("tst", 0, nil)
	fmt.Println("Incomplete:", k.Incomplete())

	k = datastore.IDKey("tst", 1, nil)
	fmt.Println("Incomplete:", k.Incomplete())
	//////////////////

	k = datastore.IDKey("tst", 0, nil)
	fmt.Println("Incomplete:", k.Incomplete())
	task := Task{
		Description: "key lime",
	}
	k, err := client.Put(Ctxx, k, &task)
	log.Println(err, k, k.Incomplete())

	/////////////////////////////////////////
	fmt.Println(k, k.Encode())
	str := k.Encode()
	k, _ = datastore.DecodeKey(str)
	fmt.Println(str, k)

	/////////////////////////////////////////
	fmt.Println(k.Kind, k.ID, k.Name, k.Parent, k.Namespace)

}
