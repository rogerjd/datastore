package main

import (
	"cloud.google.com/go/datastore"
	"fmt"
	"google.golang.org/api/iterator"
	"log"
)

type (
	person struct {
		Name string
		Age  int
		Sex  rune
	}

	pet struct {
		P    person //todo: ??
		Type string
	}

	toy struct {
		Name  string
		Price float32
	}
)

//https://medium.com/google-cloud/entity-groups-ancestors-and-indexes-in-datastore-a-working-example-3ee40cc185ee
func entityGroups() {
	prsn := person{"Lucy", 18, 'F'}
	x1 := datastore.NameKey("Person", "Lucy", nil)
	fmt.Println(x1)

	k, err := client.Put(Ctxx, x1, &prsn)
	log.Println(err, k)

	pet := pet{P: person{Name: "Cherie", Age: 7, Sex: 'F'}, Type: "Dog"}
	x2 := datastore.NameKey("Pet", "Cherie", x1)
	fmt.Println(x2)

	k, err = client.Put(Ctxx, x2, &pet)
	log.Println(err, k)

	ty := toy{"Rope", 10.99}
	x3 := datastore.NameKey("Toy", "Rope", x2)
	fmt.Println(x3)

	k, err = client.Put(Ctxx, x3, &ty)
	log.Println(err, k)

	ty = toy{"Tennis Ball", 0.99}
	x3 = datastore.NameKey("Toy", "Tennis Ball", x2)
	fmt.Println(x3)

	k, err = client.Put(Ctxx, x3, &ty)
	log.Println(err, k)

	/////////////////////////////////////////////
	fmt.Println("query")

	query := datastore.NewQuery("Toy").Ancestor(x2)
	it := client.Run(Ctxx, query)
	fmt.Println(x2.Name, "toys")
	for {
		var t toy
		_, err := it.Next(&t)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Error fetching next task: %v", err)
		}
		fmt.Printf("Name %q, Price %v\n", t.Name, t.Price)
	}
}

func addPetAndToy() {
	x1 := datastore.NameKey("Person", "Lucy", nil)
	fmt.Println(x1)
	prsn := person{}
	client.Get(Ctxx, x1, &prsn)
	fmt.Println(prsn)
	//ok we have the right person/key, Lucy

	//add pet
	pet := pet{P: person{Name: "Busby", Age: 3, Sex: 'M'}, Type: "Fish"}
	x2 := datastore.NameKey("Pet", "Busby", x1)
	fmt.Println(x2)
	k, err := client.Put(Ctxx, x2, &pet)
	log.Println(err, k)

	//add its toy
	ty := toy{"Castle", 49.99}
	x3 := datastore.NameKey("Toy", "Castle", x2)
	fmt.Println(x3)

	k, err = client.Put(Ctxx, x3, &ty)
	log.Println(err, k)
}

func qt() {
	fmt.Println("query toys")
	query := datastore.NewQuery("Toy")
	it := client.Run(Ctxx, query)
	for {
		var t toy
		key, err := it.Next(&t)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatalf("Error fetching next task: %v", err)
		}
		fmt.Printf("Name %q, Price %v\n", t.Name, t.Price)

		//get toy parent
		pet := pet{}
		client.Get(Ctxx, key.Parent, &pet)
		fmt.Printf("Name %q, Age %v Sex %v Type %v\n", pet.P.Name, pet.P.Age, string(pet.P.Sex), pet.Type)
	}
}
