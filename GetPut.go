package main

import (
	"cloud.google.com/go/datastore"
	"fmt"
	_ "google.golang.org/api/iterator"
	"log"
)

type (
	prj struct {
		Name, Status string
	}
)

//func GetPut(abc *datastore.Client) { //it's another package with its own type ???
func GetPut() {
	fmt.Println("GetPut func", N)

	kind := "Task"
	name := "sampleTask"
	taskKey := datastore.NameKey(kind, name, nil)

	task := Task{
		Description: "Buy milk, bread, banana",
	}

	x1 := datastore.IDKey("Tesk", 0, nil)
	fmt.Println(x1)
	x2 := datastore.IncompleteKey("Task", nil)
	fmt.Println(x2)

	k, err := client.Put(Ctxx, taskKey, &task)
	log.Println(err, taskKey, k)

	k, err = client.Put(Ctxx, x1, &task)
	log.Println(err, k)
	k, err = client.Put(Ctxx, x2, &task)
	log.Println(err, k)

	t := Task{}
	err = client.Get(Ctxx, k, &t)
	log.Println(err)
	log.Println(t)

	ancestor()
}

func ancestor() {
	fmt.Println("GetPut ancestor", N)

	p1 := prj{
		Name: "shopping list",
	}
	k1 := datastore.IDKey("prj", 0, nil)
	k1, err := client.Put(Ctxx, k1, &p1)

	task := Task{
		Description: "Buy milk, bread, plantain",
	}

	x1 := datastore.IDKey("Task", 0, k1)
	fmt.Println(x1)

	k, err := client.Put(Ctxx, x1, &task)
	log.Println(err, k)

	p2 := prj{}
	err = client.Get(Ctxx, k1, &p2)
	log.Println(p2, err, k1)

	t2 := Task{}
	err = client.Get(Ctxx, k, &t2)
	log.Println(t2, err, k)

	/*
		q := datastore.NewQuery("prj")
		it := client.Run(Ctxx, q)
		for {
			var p prj
			k1, err := it.Next(&p)
			if err == iterator.Done{
				break
			}
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(p, k1)
		}
	*/
}
